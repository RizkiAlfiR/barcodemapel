package com.example.barcodematapelajaran.Utils

import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.example.barcodematapelajaran.R
import com.github.ybq.android.spinkit.SpinKitView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.text.DecimalFormat
import java.util.*

fun String?.checkValue(): String{
    Log.e("Value", this)
    if (this == ""  || this == " "){
        return "-"
    }else {
        return this!!
    }
}

fun View.findText(resource: Int) : TextView {
    return this.findViewById<TextView>(resource)
}


fun View.findButton(resource: Int) : Button {
    return this.findViewById<Button>(resource)
}

fun View.findView(resource: Int) : View {
    return this.findViewById<View>(resource)
}

fun Dialog.findText(resource: Int) : TextView {
    return this.findViewById<TextView>(resource)
}

fun Dialog.setText(resource: Int, content: String?){
    this.findViewById<TextView>(resource).text = content
}

fun Dialog.findButton(resource: Int) : Button {
    return this.findViewById<Button>(resource)
}

fun Dialog.findImageButton(resource: Int) : ImageButton {
    return this.findViewById<ImageButton>(resource)
}

fun Dialog.findLayout(resource: Int) : View {
    return this.findViewById<View>(resource)
}

fun Dialog.findView(resource: Int) : View {
    return this.findViewById<View>(resource)
}

fun checkCameraHardware(context: Context): Boolean {
    return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
}

fun checkCompress(bitmap: Bitmap, maxSize: Int, scale: Double): Bitmap {
    if (bitmap.byteCount > maxSize){
        val resized = Bitmap.createScaledBitmap(bitmap, (bitmap.width/scale).toInt(), (bitmap.height/scale).toInt(), true)
        return checkCompress(resized, maxSize, scale)
    }else {
        return bitmap
    }
}

fun getEncodeString(bitmap: Bitmap): String{
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    val byteFormat = stream.toByteArray()
    return Base64.encodeToString(byteFormat, Base64.NO_WRAP)
}

fun Context.createDialog(layout: Int): Dialog {
    val dialog = Dialog(this)
    dialog.setContentView(layout)
    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setCancelable(false)
    return dialog
}

fun getNIKSize(): Int{
    return 16
}

fun View?.hide() {
    if (this != null)
        this.visibility = View.GONE
}

fun View?.visible() {
    if (this != null)
        this.visibility = View.VISIBLE
}

fun String.splitDate(): String{
    val date = this.split("T")
    return date[0].dateFormat("-")
}

fun String?.indoDateFormat(split : String): String{
    if (this != null){
        val date = this.split(split)
        return date[0] + " " + bulan(date[1].toInt()) + " " + date[2]
    }else{
        return ""
    }
}

fun TextView.checkStatusMohonBPHTB(status: String){
    this.text = status
    if (status.equals("PERMOHONAN")){
        this.setTextColor(resources.getColor(R.color.biru))
    }else if (status.equals("BAYAR")){
        this.setTextColor(resources.getColor(R.color.hijau))
    }else if (status.equals("TOLAK")){
        this.setTextColor(resources.getColor(R.color.merah))
    }
}

fun TextView.checkStatusReklame(statusTransaksi: Int, akhirj: String) : Int{
    when(statusTransaksi){
        1 -> {
            this.text = "Waiting Checkout"
            this.setTextColor(resources.getColor(R.color.birumuda))
            return 1
        }
        2 -> {
            this.text = "Waiting Pembayaran"
            this.setTextColor(resources.getColor(R.color.oranye))
            return 1
        }
        3 -> {
            this.text = "Pembayaran Sukses"
            this.setTextColor(resources.getColor(R.color.hijau))
            return 1
        }
        4 -> {
            if (akhirj.checkDateNow()){
                this.text = "Masukan ke Keranjang"
                this.setTextColor(resources.getColor(R.color.biru))
                return 2
            }else {
                this.text = "SKPD / STPD Expired"
                this.setTextColor(resources.getColor(R.color.merah))
                return 0
            }
        }
        5 -> {
            this.text = "Pembayaran Expired"
            this.setTextColor(resources.getColor(R.color.merah))
            return 0
        }
        else -> return 0
    }
}



fun TextView.checkStatusReklame(status: String){
    this.text = status
    if (status.equals("SKPD")){
        this.setTextColor(resources.getColor(R.color.biru))
    }else{
        this.setTextColor(resources.getColor(R.color.blacka1))
    }
}

fun String.dateFormat(split : String): String{
    val date = this.split(split)
    return bulan(date[1].toInt()) + " " + date[2] + ", " + date[0]
}

fun String.dateSpliter(split: String, position: Int): Int{
    return this.split(split)[position].toInt()
}

fun String.encode(): String {
    return Base64.encodeToString(this.toByteArray(), Base64.DEFAULT)
}

fun String?.decode(): String {
    return String(Base64.decode(this, Base64.DEFAULT), StandardCharsets.UTF_8)
}

fun String?.indoDateFormat2(split : String): String{
    if (this != null){
        val date = this.split(split)
        Log.e("Tanggal Convert", this)
        return date[1] + " " + bulan(date[0].toInt()) + " " + date[2]
    }else{
        return ""
    }
}

fun String?.swapDate(split: String): String {
    if (this != null){
        val date = this.split(split)
        return date[2] + split + date[0] + split + date[1]
    }else {
        return ""
    }
}

fun String.checkDateNow(): Boolean{
    val now = Calendar.getInstance(TimeZone.getDefault())
    val date = this.replace("-", "").toLong()
    val dateNow = "${now.get(Calendar.YEAR)}${now.get(Calendar.MONTH)+1}${now.get(Calendar.DAY_OF_MONTH)}".toLong()
    return date > dateNow
}

fun String.dayRemain2(): String{
    val now = Calendar.getInstance(TimeZone.getDefault())
    val thatDay = Calendar.getInstance()

    var day_remain: Long = 0

    thatDay.set(Calendar.DAY_OF_MONTH, this.dateSpliter("-", 2))
    thatDay.set(Calendar.MONTH, this.dateSpliter("-", 1) - 1)
    thatDay.set(Calendar.YEAR, this.dateSpliter("-", 0))

    day_remain = (thatDay.timeInMillis - now.getTimeInMillis()) / (24 * 60 * 60 * 1000)

    return day_remain.toString()
}

fun String.dayRemain(): String{
    val now = Calendar.getInstance(TimeZone.getDefault())
    val thatDay = Calendar.getInstance()

    var day_remain: Long = 0

    thatDay.set(Calendar.DAY_OF_MONTH, this.dateSpliter("-", 0))
    thatDay.set(Calendar.MONTH, this.dateSpliter("-", 1) - 1)
    thatDay.set(Calendar.YEAR, this.dateSpliter("-", 2))

    day_remain = (thatDay.timeInMillis - now.getTimeInMillis()) / (24 * 60 * 60 * 1000)

    return day_remain.toString()
}

fun bulan(bulan: Int): String{
    when(bulan){
        1 -> return "Januari"
        2 -> return "Februari"
        3 -> return "Maret"
        4 -> return "April"
        5 -> return "Mei"
        6 -> return "Juni"
        7 -> return "Juli"
        8 -> return "Agustus"
        9 -> return "September"
        10 -> return "Oktober"
        11 -> return "November"
        12 -> return "Desember"
        else -> return ""
    }
}

fun Int.getBulan(): String{
    when(this){
        1 -> return "Januari"
        2 -> return "Februari"
        3 -> return "Maret"
        4 -> return "April"
        5 -> return "Mei"
        6 -> return "Juni"
        7 -> return "Juli"
        8 -> return "Agustus"
        9 -> return "September"
        10 -> return "Oktober"
        11 -> return "November"
        12 -> return "Desember"
        else -> return ""
    }
}

fun ArrayList<String>.checkOmset(omset: Long, percent: String){
    var check = true
    for (item in this){
        if (item.equals(percent)){
            check = false
            break
        }
    }
    if (omset > 0){
        if (check){
            this.add(percent)
        }
    }else {
        this.remove(percent)
    }
}

fun ArrayList<String>.checkOmset(omset: Long){
    if (omset > 0){
        this.add(omset.moneyFormat() + " ")
    }else {
        this.remove(omset.moneyFormat() + " ")
    }
}

fun ArrayList<String>.addOmset(omset: Long){
    if (omset > 0){
        this.add(omset.toString())
    }else {
        this.remove(omset.toString())
    }
}

fun Long.moneyFormat(): String {
    val formatter = DecimalFormat("#,###")
    return "Rp. " +formatter.format(this)
}

fun String?.moneyFormat(): String {
    if (this != null){
        val formatter = DecimalFormat("#,###")
        return "Rp. " +formatter.format(this.toLong())
    }else {
        return ""
    }
}

fun randomNumber(): Int{
    val min = 1
    val max = 100
    val random = Random().nextInt(max - min + 1) + min
    return random
}

fun Int.makeTen(): String{
    if (this < 10){
        return "0" + this
    }else {
        return this.toString()
    }
}

fun ImageView.setImageUrl(url : String?){
    if (url != null){
        Picasso.get().load(url).into(this)
    }
}

fun ImageView.setImageUrl(url : String?, spinner: SpinKitView) {
    if (url != null) {
        Picasso.get().load(url).into(this, getCallBack(this, spinner))
    }
}

fun getCallBack(imageView: ImageView, spinnerView: SpinKitView): Callback {
    spinnerView.visible()
    return object : Callback {
        override fun onSuccess() {
            spinnerView.hide()
        }
        override fun onError(e: Exception?) {
            spinnerView.hide()
            imageView.setImageResource(R.color.blacka1)
        }
    }
}