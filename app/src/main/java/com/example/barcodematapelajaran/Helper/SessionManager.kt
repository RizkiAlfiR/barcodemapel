package com.example.barcodematapelajaran.Helper

import android.content.Context
import android.util.Log
import com.example.barcodematapelajaran.BuildConfig
import com.example.barcodematapelajaran.Utils.decode
import com.example.barcodematapelajaran.Utils.encode
import com.example.barcodematapelajaran.Utils.makeTen
import java.util.*

class SessionManager(var context: Context) {
    private  var pref = context.getSharedPreferences(BuildConfig.CONF_NAME, Context.MODE_PRIVATE)
    var editor = pref.edit()

    private fun getTimeLogout(): Int {
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        calendar.add(Calendar.DAY_OF_YEAR, 1)

        val year = calendar.get(Calendar.YEAR).toString()
        val month = (calendar.get(Calendar.MONTH) + 1).toString()
        val day_month = calendar.get(Calendar.DAY_OF_MONTH).toString()

        val all = year + Integer.parseInt(month).makeTen() + Integer.parseInt(day_month).makeTen()
        return all.toInt()
    }

    private fun setLogin(isLoggedIn: Boolean) {
        editor.putBoolean(BuildConfig.CONF_STATUS, isLoggedIn)
        editor.commit()
    }

    private fun setKey(bearer: String) {
        editor.putString(BuildConfig.CONF_KEY, bearer.encode())
        editor.commit()
    }

    private fun setTimeLogout(time : Int) {
        editor.putInt(BuildConfig.CONF_TIME_LOGIN, time)
        editor.commit()
    }

    fun checkTimeLogout() {
        val calendar = Calendar.getInstance(TimeZone.getDefault())

        val year = calendar.get(Calendar.YEAR).toString()
        val month = (calendar.get(Calendar.MONTH) + 1).toString()
        val day_month = calendar.get(Calendar.DAY_OF_MONTH).toString()

        val all = year + Integer.parseInt(month).makeTen() + Integer.parseInt(day_month).makeTen()
        val now = Integer.parseInt(all)

        Log.e("Time Logout " , pref.getInt(BuildConfig.CONF_TIME_LOGIN, 0).toString())
        Log.e("Time Logout NOW " , now.toString())

        if (pref.getInt(BuildConfig.CONF_TIME_LOGIN, 0) <= now) {
            logout()
        }
    }

    fun login(code:String){
        setTimeLogout(getTimeLogout())
        setLogin(true)
        setKey(code)
    }

    fun isLoggedIn(): Boolean {
        checkTimeLogout()
        return pref.getBoolean(BuildConfig.CONF_STATUS, false)
    }

    fun getBearerToken(): String {
        return pref.getString(BuildConfig.CONF_KEY, "").decode()
    }

    fun getBearerUser(): String {
        return pref.getString(BuildConfig.CONF_NAMA, "").decode()
    }

    fun logout() {
        setTimeLogout(0)
        setLogin(false)
        setKey("")
    }
}