package com.example.barcodematapelajaran.Helper

import android.content.Context
import com.example.barcodematapelajaran.BuildConfig
import com.example.barcodematapelajaran.Utils.decode
import com.example.barcodematapelajaran.Utils.encode

class ProfileManager(var context: Context) {
    private  var pref = context.getSharedPreferences(BuildConfig.CONF_NAME, Context.MODE_PRIVATE)
    var editor = pref.edit()

    private fun setUser(user: String) {
        editor.putString(BuildConfig.CONF_NAMA, user.encode())
        editor.commit()
    }

    fun profile(code:String){
        setUser(code)
    }

    fun getBearerUser(): String {
        return pref.getString(BuildConfig.CONF_NAMA, "").decode()
    }
}