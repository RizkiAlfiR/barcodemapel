package com.example.barcodematapelajaran.Client.Response

data class CheckQRResponse (
    val detail: Detail,
    val msg: String,
    val sttCode: String
)