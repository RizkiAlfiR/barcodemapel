package com.example.barcodematapelajaran.Client.Response

data class Detail (
    val NIP: String,
    val Nama: String,
    val MataPelajaran: String,
    val Jurusan: String,
    val Kelas: String,
    val TanggalJam: String,
    val Keterangan: String
)