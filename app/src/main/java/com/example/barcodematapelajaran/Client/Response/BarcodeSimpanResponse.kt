package com.example.barcodematapelajaran.Client.Response

data class BarcodeSimpanResponse (
    val sttCode: String,
    val msg: String
)