package com.example.barcodematapelajaran.Client.Request

data class CheckQRRequest (val idQr: String, val username: String)