package com.example.barcodematapelajaran.Client.Request

data class BarcodeSimpanRequest (
    val username: String,
    val idQR: String,
    val Status: String
)