package com.example.barcodematapelajaran.Client

import com.example.barcodematapelajaran.BuildConfig
import com.example.barcodematapelajaran.Client.Request.BarcodeSimpanRequest
import com.example.barcodematapelajaran.Client.Request.CheckQRRequest
import com.example.barcodematapelajaran.Client.Request.LoginRequest
import com.example.barcodematapelajaran.Client.Response.BarcodeSimpanResponse
import com.example.barcodematapelajaran.Client.Response.CheckQRResponse
import com.example.barcodematapelajaran.Client.Response.LoginResponse
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterface {
    @POST("Account/LoginUser")
    fun postLogin(@Header("Accept") accept: String,
                  @Header("Content-Type") content: String,
                  @Body request: LoginRequest): Call<LoginResponse>

    @POST("ScanQR/ScanMulai")
    fun checkQR(@Header("Accept") accept: String,
                @Header("Content-Type") content: String,
                @Body request: CheckQRRequest): Call<CheckQRResponse>

    @POST("ScanQR/Simpan")
    fun submitQR(@Header("Accept") accept: String,
                @Header("Content-Type") content: String,
                @Body request: BarcodeSimpanRequest): Call<BarcodeSimpanResponse>

    companion object {

        fun createAPI(): ApiInterface {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(StethoInterceptor())
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL_DEV)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}