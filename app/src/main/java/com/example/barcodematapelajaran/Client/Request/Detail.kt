package com.example.barcodematapelajaran.Client.Request

data class Detail (
    val NIP: String,
    val Nama: String,
    val MataPelajaran: String,
    val Jurusan: String,
    val Kelas: String,
    val TanggalJam: String,
    val Keterangan: String
)