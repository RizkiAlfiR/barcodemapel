package com.example.barcodematapelajaran.Modul.Menu.BarcodeMulaiPelajaran.View

interface BarcodeMulaiPelajaranView {
    fun showLoading()
    fun hideLoading()

    fun showQRResult(data: Any)
    fun showQRError(msg: String)
    fun showError(msg: String)
    fun showResult(result: Any)
}