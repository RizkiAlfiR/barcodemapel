package com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.View

interface ScanQRView {
    fun showLoading()
    fun hideLoading()

    fun showQRResult(data: Any)
//    fun showQRInput(id: String)
    fun showQRError(msg: String)
}