package com.example.barcodematapelajaran.Modul.Login.View

interface LoginView {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showResult(result: Any)
    fun showProfile(result: Any)
}