package com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.Presenter

import android.content.Context
import com.example.barcodematapelajaran.Client.ApiInterface
import com.example.barcodematapelajaran.Client.Request.CheckQRRequest
import com.example.barcodematapelajaran.Client.Response.CheckQRResponse
import com.example.barcodematapelajaran.Helper.SessionManager
import com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.View.ScanQRView
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class ScanQRPresenter(val context: Context, val view: ScanQRView) {

    private lateinit var sessionManager: SessionManager

    private fun initialize(){
        sessionManager = SessionManager(context)
    }

    fun checkData(code: String){
        if(code.equals("")) {
            view.showQRError("Kode QR anda kosong")
        }else {
            postCheckData(code)
        }
    }

    fun postCheckData(code: String){

        view.showLoading()
        initialize()

        val username = sessionManager.getBearerToken()
        val request = CheckQRRequest(code, username)

        ApiInterface.createAPI().checkQR("application/json","application/json", request).enqueue(object: Callback, retrofit2.Callback<CheckQRResponse> {
            override fun onResponse(call: Call<CheckQRResponse>, response: Response<CheckQRResponse>) {
                view.hideLoading()
                if (response.isSuccessful){
                    val body = response.body()

                    if(body?.sttCode == "200") {
                        view.showQRResult(body.detail)
                    }else {
                        view.showQRError(body!!.msg)
                    }

                }else {
                    view.showQRError("Gagal mengakses server")
                }
            }
            override fun onFailure(call: Call<CheckQRResponse>, t: Throwable) {
                view.hideLoading()
                view.showQRError("Connection Time Out")
            }
        })
    }

}