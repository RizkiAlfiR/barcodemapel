package com.example.barcodematapelajaran.Modul.Login.Activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.barcodematapelajaran.Helper.ProfileManager
import com.example.barcodematapelajaran.Helper.SessionManager
import com.example.barcodematapelajaran.Modul.Home.Activity.MainActivity
import com.example.barcodematapelajaran.Modul.Login.Presenter.LoginPresenter
import com.example.barcodematapelajaran.Modul.Login.View.LoginView
import com.example.barcodematapelajaran.R
import com.example.barcodematapelajaran.Utils.createDialog
import com.example.barcodematapelajaran.Utils.findLayout
import com.example.barcodematapelajaran.Utils.findText
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity

class LoginActivity : AppCompatActivity(), LoginView {

    lateinit var dialogLoading: Dialog
    lateinit var dialogFailed: Dialog

    lateinit var presenter: LoginPresenter
    lateinit var sessionManager: SessionManager
    lateinit var profileManager: ProfileManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        dialogLoading = createDialog(R.layout.dialog_loading)
        dialogFailed = createDialog(R.layout.dialog_failed)

        sessionManager = SessionManager(this)
        profileManager = ProfileManager(this)
        presenter = LoginPresenter(this)

        if (sessionManager.isLoggedIn()){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnLogin.setOnClickListener {
//            startActivity<MainActivity>()
            val username = edtUsername.text.toString().trim()
            val password = edtPassword.text.toString().trim()
            presenter.checkLogin(username, password)
        }
    }

    override fun showLoading() {
        val txtLoading = dialogLoading.findText(R.id.txtLoading)
        txtLoading.text = "Authenticating..."
        dialogLoading.show()
    }

    override fun hideLoading() {
        dialogLoading.hide()
    }

    override fun onResume() {
        super.onResume()
        try {
//            CheckVersion(applicationContext, this).execute()
            SessionManager(this)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun showError(msg: String) {
        val txtMsg = dialogFailed.findText(R.id.txtMessage)
        val btnClose = dialogFailed.findLayout(R.id.btnSubmit)
        txtMsg.text = msg

        dialogFailed.show()
        btnClose.setOnClickListener {
            dialogFailed.hide()
        }
    }

    override fun showResult(result: Any) {
        val msg: String = result as String
        sessionManager.login(msg)

        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun showProfile(result: Any) {
        val prfl: String = result as String
        profileManager.profile(prfl)
    }
}
