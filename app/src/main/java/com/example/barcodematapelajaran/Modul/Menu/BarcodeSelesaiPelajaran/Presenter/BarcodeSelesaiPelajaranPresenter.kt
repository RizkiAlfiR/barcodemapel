package com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.Presenter

import android.content.Context
import com.example.barcodematapelajaran.Client.ApiInterface
import com.example.barcodematapelajaran.Client.Request.BarcodeSimpanRequest
import com.example.barcodematapelajaran.Client.Request.CheckQRRequest
import com.example.barcodematapelajaran.Client.Response.BarcodeSimpanResponse
import com.example.barcodematapelajaran.Client.Response.CheckQRResponse
import com.example.barcodematapelajaran.Helper.SessionManager
import com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.View.BarcodeSelesaiPelajaranView
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class BarcodeSelesaiPelajaranPresenter(val context: Context, val view: BarcodeSelesaiPelajaranView) {

    private lateinit var sessionManager: SessionManager

    private fun initialize(){
        sessionManager = SessionManager(context)
    }

    fun postCheckData(code: String){

        view.showLoading()
        initialize()

        val username = sessionManager.getBearerToken()
        val request = CheckQRRequest(code, username)

        ApiInterface.createAPI().checkQR("application/json","application/json", request).enqueue(object: Callback, retrofit2.Callback<CheckQRResponse> {
            override fun onResponse(call: Call<CheckQRResponse>, response: Response<CheckQRResponse>) {
                view.hideLoading()
                if (response.isSuccessful){
                    val body = response.body()

                    if(body?.sttCode == "200") {
                        view.showQRResult(body.detail)
                    }else {
                        view.showQRError(body!!.msg)
                    }

                }else {
                    view.showQRError("Gagal mengakses server")
                }
            }
            override fun onFailure(call: Call<CheckQRResponse>, t: Throwable) {
                view.hideLoading()
                view.showQRError("Connection Time Out")
            }
        })
    }

    fun checkData(code: String){
        if(code.equals("")) {
            view.showQRError("Kode QR anda kosong")
        }else {
            postCheckData(code)
        }
    }

    fun simpanData(code: String) {
        view.showLoading()
        val sessionManager = SessionManager(context)
        val username = sessionManager.getBearerToken()

        val request = BarcodeSimpanRequest(username, code, "Selesai")
        ApiInterface.createAPI().submitQR("application/json","application/json", request).enqueue(object: Callback, retrofit2.Callback<BarcodeSimpanResponse> {
            override fun onResponse(call: Call<BarcodeSimpanResponse>, response: Response<BarcodeSimpanResponse>) {
                view.hideLoading()
                if (response.isSuccessful){
                    if (response.body()!!.sttCode.equals("200")){
                        view.showResult(response.body()!!)
//                        saveQR(kode, response.body()!!.Nop)
                    }else {
                        view.showError(response.body()!!.msg)
                    }
                }else {
                    view.showError("Gagal terhubung ke server")
                }
            }

            override fun onFailure(call: Call<BarcodeSimpanResponse>, t: Throwable) {
                view.hideLoading()
                view.showError( "Connection Time Out")
            }
        })
    }

}