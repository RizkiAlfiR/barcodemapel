package com.example.barcodematapelajaran.Modul.Login.Presenter

import com.example.barcodematapelajaran.Client.ApiInterface
import com.example.barcodematapelajaran.Client.Request.LoginRequest
import com.example.barcodematapelajaran.Client.Response.LoginResponse
import com.example.barcodematapelajaran.Modul.Login.View.LoginView
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class LoginPresenter(val view: LoginView) {
    fun checkLogin(username: String, password: String){
        if(username.isEmpty() && password.isEmpty()){
            view.showError("Pastikan form tidak boleh kosong")
        }else {
//            if (username.toLowerCase().equals("ishom") && password.toLowerCase().equals("123123"))
//            view.showError("CheckLogin Pressed")
            doLogin(username, password)
        }
    }

    private fun doLogin(username: String, password: String){
        view.showLoading()
        val request = LoginRequest(username, password)
        ApiInterface.createAPI().postLogin("application/json","application/json", request).enqueue(object: Callback, retrofit2.Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                view.hideLoading()
                if (response.isSuccessful){
                    val body = response.body()!!
                    if (body.sttCode.equals("200")){
                        view.showResult(username)
                        val nama = body.nama
                        view.showProfile(nama)
                    }else {
                        view.showError(body.msg)
                    }
                }else {
                    view.showError("Error 404")
                }
            }
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                view.hideLoading()
                view.showError(t.message.toString())
            }
        })
    }
}