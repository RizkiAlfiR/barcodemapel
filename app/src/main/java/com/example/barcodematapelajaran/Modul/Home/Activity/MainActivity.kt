package com.example.barcodematapelajaran.Modul.Home.Activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.barcodematapelajaran.Helper.ProfileManager
import com.example.barcodematapelajaran.Helper.SessionManager
import com.example.barcodematapelajaran.Modul.Home.Presenter.ProfilePresenter
import com.example.barcodematapelajaran.Modul.Home.View.ProfileView
import com.example.barcodematapelajaran.Modul.Login.Activity.LoginActivity
import com.example.barcodematapelajaran.Modul.Login.Presenter.LoginPresenter
import com.example.barcodematapelajaran.Modul.Menu.BarcodeMulaiPelajaran.Activity.BarcodeMulaiPelajaranActivity
import com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.Activity.BarcodeSelesaiPelajaranActivity
import com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.Activity.ScanQRActivity
import com.example.barcodematapelajaran.R
import com.example.barcodematapelajaran.Utils.createDialog
import com.example.barcodematapelajaran.Utils.findLayout
import com.example.barcodematapelajaran.Utils.setText
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(), ProfileView {

    lateinit var sessionManager: SessionManager
    lateinit var profileManager: ProfileManager
    lateinit var presenter: ProfilePresenter

    lateinit var dialogLogout: Dialog
    lateinit var dialogLoading: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = ProfilePresenter(this)
        sessionManager = SessionManager(this)
        sessionManager.checkTimeLogout()
        profileManager = ProfileManager(this)

        dialogLoading = createDialog(R.layout.dialog_loading)
        dialogLogout = createDialog(R.layout.dialog_konfirmasi)

        txtNama.text = "Welcome, ${profileManager.getBearerUser()}"
//        txtNama.text = "Selamat datang, "
        txtID.text = "NIP. ${sessionManager.getBearerToken()}"

        bgBarcodeMulai.setOnClickListener {
            startActivity<BarcodeMulaiPelajaranActivity>("kode" to 1)
//            startActivity<ScanQRActivity>("kode" to 1)
        }

        bgBarcodeSelesai.setOnClickListener {
            startActivity<BarcodeSelesaiPelajaranActivity>("kode" to 1)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.setTitle("")
    }

    override fun onDestroy() {
        super.onDestroy()
        dialogLoading.dismiss()
        dialogLogout.dismiss()
    }

    override fun showLoading() {
        dialogLoading.show()
    }

    override fun hideLoading() {
        dialogLoading.hide()
    }

    override fun onResume() {
        super.onResume()
        try {
//            CheckVersion(applicationContext, this).execute()
            SessionManager(this)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
    }

//    override fun showResult(result: Any) {
//        val data = result as ProfileResponse
//        txtNama.text = data.NAMA
//        txtID.text = "NIP. ${data.NIP}"
//    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_logout, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.btnLogout){
//            displayDialogLogout()
            sessionManager.logout()
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

//    private fun displayDialogLogout(){
//        dialogLogout.setText(R.id.txtJudul, "Pesan Dialog")
//        dialogLogout.setText(R.id.txtMessage, "Apakah anda ingin logout dari aplikasi?")
//
//        val btnYa = dialogLogout.findLayout(R.id.btnYa)
//        val btnTidak = dialogLogout.findLayout(R.id.btnTidak)
//
//        btnYa.setOnClickListener {
//            sessionManager.logout()
//            val intent = Intent(this, LoginActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//            startActivity(intent)
//        }
//
//        btnTidak.setOnClickListener {
//            dialogLogout.hide()
//        }
//
//        dialogLogout.show()
//        startActivity<LoginActivity>()
//    }
}
