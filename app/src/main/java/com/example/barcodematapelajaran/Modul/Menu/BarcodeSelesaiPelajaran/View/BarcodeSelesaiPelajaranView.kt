package com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.View

interface BarcodeSelesaiPelajaranView {
    fun showLoading()
    fun hideLoading()

    fun showQRResult(data: Any)
    fun showQRError(msg: String)
    fun showError(msg: String)
    fun showResult(result: Any)
}