package com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.Activity

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import com.example.barcodematapelajaran.Client.Response.Detail
import com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.Presenter.ScanQRPresenter
import com.example.barcodematapelajaran.Modul.Menu.ScanQRCode.View.ScanQRView
import com.example.barcodematapelajaran.R
import com.example.barcodematapelajaran.Utils.createDialog
import com.example.barcodematapelajaran.Utils.findImageButton
import com.example.barcodematapelajaran.Utils.findText
import com.example.barcodematapelajaran.Utils.findView
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class ScanQRActivity : AppCompatActivity(), BarcodeCallback, ScanQRView {

    private lateinit var barcodeScanner : DecoratedBarcodeView
    private var CAMERA_REQUEST_CODE = 200

    private lateinit var presenter: ScanQRPresenter

    private lateinit var dialogLoading: Dialog
    private lateinit var dialogFailed: Dialog
    private lateinit var dialogQRValid: Dialog
    private lateinit var dialogShowMapel: Dialog

    var intent_code = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr)

        barcodeScanner = findViewById(R.id.content_frame)
        presenter = ScanQRPresenter(this, this)

        val bundle = intent.extras
        try {
            intent_code = bundle.getInt("kode")
        }catch (e: Exception){
            intent_code = 2
            Log.e("Scan QR", e.message.toString())
        }

        dialogLoading = createDialog(R.layout.dialog_loading)
        dialogFailed = createDialog(R.layout.dialog_failed)
        dialogQRValid = createDialog(R.layout.dialog_success)
        dialogShowMapel = createDialog(R.layout.dialog_detail_mapel)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        barcodeScanner.decodeSingle(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.CAMERA),CAMERA_REQUEST_CODE)
        }
        barcodeScanner.resume()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE){
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
//                toast(resources.getString(R.string.error_kamera_permission)).show()
                toast(resources.getString(R.string.error_kamera_permission))
                finish()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        barcodeScanner.pause()
    }

    override fun barcodeResult(result: BarcodeResult?) {
        val code = result?.text
        presenter.checkData(code!!)
        barcodeScanner.pause()
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
    }

    override fun showLoading() {
        dialogLoading.findText(R.id.txtLoading).text = "Sedang membaca kode QR..."
        dialogLoading.show()
    }

    override fun hideLoading() {
        dialogLoading.hide()
    }

    override fun showQRResult(data: Any) {

        val mapel = data as Detail

        if(intent_code == 1) {
            val txtNIP = dialogShowMapel.findText(R.id.txtNIP)
            val txtNama = dialogShowMapel.findText(R.id.txtNama)
            val txtMapel = dialogShowMapel.findText(R.id.txtMapel)
            val txtJurusan = dialogShowMapel.findText(R.id.txtJurusan)
            val txtKelas = dialogShowMapel.findText(R.id.txtKelas)
            val txtTanggalJam = dialogShowMapel.findText(R.id.txtTglJam)
            val txtKeterangan = dialogShowMapel.findText(R.id.txtKeterangan)
            val btnClose = dialogShowMapel.findImageButton(R.id.btnClose)

            txtNIP.text = "NIP. " + mapel.NIP
            txtNama.text = mapel.Nama
            txtMapel.text = mapel.MataPelajaran
            txtJurusan.text = mapel.Jurusan
            txtKelas.text = mapel.Kelas
            txtTanggalJam.text = mapel.TanggalJam
            txtKeterangan.text = mapel.Keterangan

            btnClose.setOnClickListener {
                dialogShowMapel.hide()
                barcodeScanner.resume()
                barcodeScanner.decodeSingle(this)
            }

            dialogShowMapel.show()
        }else {
            val txtMessage = dialogFailed.findText(R.id.txtMessage)
            val btnClose = dialogFailed.findView(R.id.btnSubmit)

            txtMessage.text = "Kode QR ini sudah digunakan oleh : ${mapel.MataPelajaran}"
            btnClose.setOnClickListener {
                dialogFailed.hide()
                barcodeScanner.resume()
                barcodeScanner.decodeSingle(this)

            }

            dialogFailed.show()
        }
    }

    override fun showQRError(msg: String) {
        val txtMessage = dialogFailed.findText(R.id.txtMessage)
        val btnClose = dialogFailed.findView(R.id.btnSubmit)

        txtMessage.text = msg
        btnClose.setOnClickListener {
            dialogFailed.hide()
            barcodeScanner.resume()
            barcodeScanner.decodeSingle(this)

        }

        dialogFailed.show()
    }

}
