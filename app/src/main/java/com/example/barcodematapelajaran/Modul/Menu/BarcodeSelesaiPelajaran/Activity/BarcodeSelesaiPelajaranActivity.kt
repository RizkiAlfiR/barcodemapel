package com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.Activity

import android.Manifest
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import com.example.barcodematapelajaran.Client.Response.Detail
import com.example.barcodematapelajaran.Helper.SessionManager
import com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.Presenter.BarcodeSelesaiPelajaranPresenter
import com.example.barcodematapelajaran.Modul.Menu.BarcodeSelesaiPelajaran.View.BarcodeSelesaiPelajaranView
import com.example.barcodematapelajaran.R
import com.example.barcodematapelajaran.Utils.*
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class BarcodeSelesaiPelajaranActivity : AppCompatActivity(), BarcodeCallback, BarcodeSelesaiPelajaranView {

    private lateinit var barcodeScanner : DecoratedBarcodeView
    private var CAMERA_REQUEST_CODE = 200

    lateinit var sessionManager: SessionManager

    private lateinit var presenter: BarcodeSelesaiPelajaranPresenter

    private lateinit var dialogLoading: Dialog
    private lateinit var dialogFailed: Dialog
    private lateinit var dialogShowMapel: Dialog
    lateinit var successDialog: Dialog

    var intent_code = 1
    var barcode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode_selesai_pelajaran)

        barcodeScanner = findViewById(R.id.content_frame)
        sessionManager = SessionManager(this)
        presenter = BarcodeSelesaiPelajaranPresenter(this, this)

        val bundle = intent.extras
        try {
            intent_code = bundle.getInt("kode")
        }catch (e: Exception){
            intent_code = 2
            Log.e("Scan QR", e.message.toString())
        }

        dialogLoading = createDialog(R.layout.dialog_loading)
        dialogFailed = createDialog(R.layout.dialog_failed)
        dialogShowMapel = createDialog(R.layout.dialog_detail_mapel)
        successDialog = createDialog(R.layout.dialog_success)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        barcodeScanner.decodeSingle(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(arrayOf(Manifest.permission.CAMERA),CAMERA_REQUEST_CODE)
        }
        barcodeScanner.resume()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_REQUEST_CODE){
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
//                toast(resources.getString(R.string.error_kamera_permission)).show()
                toast(resources.getString(R.string.error_kamera_permission))
                finish()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        barcodeScanner.pause()
    }

    override fun barcodeResult(result: BarcodeResult?) {
        val code = result?.text
        presenter.checkData(code!!)
        barcodeScanner.pause()
        barcode = code
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
    }

    override fun showLoading() {
        dialogLoading.findText(R.id.txtLoading).text = "Sedang membaca kode QR..."
        dialogLoading.show()
    }

    override fun hideLoading() {
        dialogLoading.hide()
    }

    override fun showQRResult(data: Any) {

        val mapel = data as Detail

        if(intent_code == 1) {
            val txtNIP = dialogShowMapel.findText(R.id.txtNIP)
            val txtNama = dialogShowMapel.findText(R.id.txtNama)
            val txtMapel = dialogShowMapel.findText(R.id.txtMapel)
            val txtJurusan = dialogShowMapel.findText(R.id.txtJurusan)
            val txtKelas = dialogShowMapel.findText(R.id.txtKelas)
            val txtTanggalJam = dialogShowMapel.findText(R.id.txtTglJam)
            val txtKeterangan = dialogShowMapel.findText(R.id.txtKeterangan)
            val btnClose = dialogShowMapel.findImageButton(R.id.btnClose)
            val btnSubmit = dialogShowMapel.findButton(R.id.btnSubmit)

            txtNIP.text = "NIP. " + mapel.NIP
            txtNama.text = mapel.Nama
            txtMapel.text = mapel.MataPelajaran
            txtJurusan.text = mapel.Jurusan
            txtKelas.text = mapel.Kelas
            txtTanggalJam.text = mapel.TanggalJam
            txtKeterangan.text = mapel.Keterangan

            btnClose.setOnClickListener {
                dialogShowMapel.hide()
                barcodeScanner.resume()
                barcodeScanner.decodeSingle(this)
            }

            btnSubmit.setOnClickListener {
                val kode = barcode

                presenter.simpanData(kode)
//                toast("Submit Mapel Mulai Pressed")
//                startActivity<MainActivity>()
            }

            dialogShowMapel.show()
        }else {
            val txtMessage = dialogFailed.findText(R.id.txtMessage)
            val btnClose = dialogFailed.findView(R.id.btnSubmit)

            txtMessage.text = "Kode QR ini sudah digunakan oleh : ${mapel.MataPelajaran}"
            btnClose.setOnClickListener {
                dialogFailed.hide()
                barcodeScanner.resume()
                barcodeScanner.decodeSingle(this)

            }

            dialogFailed.show()
        }
    }

    override fun showQRError(msg: String) {
        val txtMessage = dialogFailed.findText(R.id.txtMessage)
        val btnClose = dialogFailed.findView(R.id.btnSubmit)

        txtMessage.text = msg
        btnClose.setOnClickListener {
            dialogFailed.hide()
            barcodeScanner.resume()
            barcodeScanner.decodeSingle(this)

        }

        dialogFailed.show()
    }

    override fun showResult(result: Any) {
//        val hasil = result as PendaftaranNOPResponse

        successDialog.hide()
        successDialog.show()

        successDialog.setText(R.id.txtMessage, "Berhasil melakukan penyimpanan barcode Mata Pelajaran")

        val btnSelesai = successDialog.findLayout(R.id.btnSubmit)

        btnSelesai.setOnClickListener {
            successDialog.hide()
            finish()
        }
    }

    override fun showError(msg: String) {
        dialogFailed.hide()
        dialogFailed.show()

        val txtMessage = dialogFailed.findViewById<TextView>(R.id.txtMessage)
        val btnClose = dialogFailed.findViewById<LinearLayout>(R.id.btnSubmit)

        txtMessage.text = msg
        btnClose.setOnClickListener {
            dialogFailed.hide()
        }
    }

}
